package org.soccer.scraper;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author Vitalii
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class MyScoreScraperTest {

    @Autowired
    private MyScoreScraper scoreScraper;

    @Test
    public void test() {
        scoreScraper.scrape();
    }
}