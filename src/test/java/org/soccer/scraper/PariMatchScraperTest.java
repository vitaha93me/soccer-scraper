package org.soccer.scraper;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.soccer.entities.Bet;
import org.soccer.repositories.BetRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.List;

/**
 * @author Vitalii
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class PariMatchScraperTest {

    @Autowired
    private PariMatchScraper pariMatchScraper;

    @Autowired
    private BetRepository betRepository;


    @Test
    public void test() {
        try {
            List<Bet> scrape = pariMatchScraper.scrape();
            betRepository.deleteAll();
            betRepository.save(scrape);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}