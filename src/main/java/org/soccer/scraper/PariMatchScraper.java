package org.soccer.scraper;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.soccer.entities.Bet;
import org.soccer.services.JsoupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import static org.apache.commons.lang3.StringUtils.containsIgnoreCase;
import static org.soccer.scraper.ParimatchConstants.*;

/**
 * @author Vitalii
 */
@Component
public class PariMatchScraper {

    private final JsoupService jsoupService;

    @Autowired
    public PariMatchScraper(JsoupService jsoupService) {
        this.jsoupService = jsoupService;
    }

    public List<Bet> scrape() throws IOException {

        Document document = jsoupService.getDocument(PARIMATCH_URL);

        List<Bet> bets = new ArrayList<>();
        for (Element element : document.select(TABLE_CSS)) {
            String liga = element.select(LIGA_CSS).text();
            if (containsIgnoreCase(liga, "СТАТИСТИКА") ||
                    !containsIgnoreCase(liga, "ФУТБОЛ")) {
                continue;
            }
            for (Element line : element.select(LINE_CSS)) {
                Bet bet = parseBetLine(line);
                if (bet == null) continue;

                bet.setLiga(liga);
                bets.add(bet);
            }
        }
        return bets;
    }

    private Bet parseBetLine(Element line) {
        String date = line.select(DATE_CSS).text();
        Elements event = line.select(EVENT_CSS);
        event.select("img").remove();
        event.select("small").unwrap();
        String foraFirst = line.select(FORA_FIRST_CSS).text();
        String foraSecond = line.select(FORA_SECOND_CSS).text();
        String foraFirstKf = line.select(FORA_FIRST_KF_CSS).text();
        String foraSecondKf = line.select(FORA_SECOND_KF_CSS).text();
        String total = line.select(TOTAL_CSS).text();
        String totalMore = line.select(TOTAL_MORE_CSS).text();
        String totalLess = line.select(TOTAL_LESS_CSS).text();
        String winFirst = line.select(WIN_FIRST_CSS).text();
        String draw = line.select(DRAW_CSS).text();
        String winSecond = line.select(WIN_SECOND_CSS).text();
        String winFirstDraw = line.select(WIN_FIRST_DRAW_CSS).text();
        String someoneWin = line.select(SOMEONE_WIN_CSS).text();
        String winSecondDraw = line.select(WIN_SECOND_DRAW_CSS).text();
        String individualTotalFirst = line.select(INDIVIDUAL_TOTAL_FIRST_CSS).text();
        String individualTotalSecond = line.select(INDIVIDUAL_TOTAL_SECOND_CSS).text();
        String individualTotalFirstMore = line.select(INDIVIDUAL_TOTAL_FIRST_MORE_CSS).text();
        String individualTotalFirstLess = line.select(INDIVIDUAL_TOTAL_FIRST_LESS_CSS).text();
        String individualTotalSecondMore = line.select(INDIVIDUAL_TOTAL_SECOND_MORE_CSS).text();
        String individualTotalSecondLess = line.select(INDIVIDUAL_TOTAL_SECOND_LESS_CSS).text();

        Bet bet = new Bet();
        try {
            bet.setDate(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        bet.setForaFirst(foraFirst);
        bet.setForaSecond(foraSecond);
        bet.setForaFirstKf(foraFirstKf);
        bet.setForaSecondKf(foraSecondKf);

        String[] split = event.html().split("<br>");
        if (split.length < 1) {
            return null;
        }

        bet.setTeamFirst(split[0]);
        bet.setTeamSecond(split[1]);
        bet.setTotal(total);
        bet.setTotalMore(totalMore);
        bet.setTotalLess(totalLess);
        bet.setWinFirst(winFirst);
        bet.setWinSecond(winSecond);
        bet.setDraw(draw);
        bet.setWinFirstOrDraw(winFirstDraw);
        bet.setSomeoneWin(someoneWin);
        bet.setWinSecondOrDraw(winSecondDraw);
        bet.setIndividualTotalFirst(individualTotalFirst);
        bet.setIndividualTotalMoreSecond(individualTotalSecond);
        bet.setIndividualTotalMoreFirst(individualTotalFirstMore);
        bet.setIndividualTotalLessFirst(individualTotalFirstLess);
        bet.setIndividualTotalMoreSecond(individualTotalSecondMore);
        bet.setIndividualTotalLessSecond(individualTotalSecondLess);

        return bet;
    }
}
