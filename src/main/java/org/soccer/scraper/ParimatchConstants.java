package org.soccer.scraper;

/**
 * @author Vitalii
 */
public class ParimatchConstants {

    public static final String PARIMATCH_URL = "https://www.parimatch.com/bet.html?filter=all";

    public static final String TABLE_CSS = ".container:has(table[rules=groups]:contains(iТ))";
    public static final String LIGA_CSS = "h3";
    public static final String LINE_CSS = ".row1:not(*[style])";

    public static final String DATE_CSS = ".bk td:nth-of-type(2)";
    public static final String EVENT_CSS = ".bk td:nth-of-type(3) a";
    public static final String FORA_FIRST_CSS = ".bk td:nth-of-type(4) b:nth-of-type(1)";
    public static final String FORA_SECOND_CSS = ".bk td:nth-of-type(4) b:nth-of-type(2)";
    public static final String FORA_FIRST_KF_CSS = ".bk td:nth-of-type(5) u:nth-of-type(1)";
    public static final String FORA_SECOND_KF_CSS = ".bk td:nth-of-type(5) u:nth-of-type(2)";
    public static final String TOTAL_CSS = ".bk td:nth-of-type(6)";
    public static final String TOTAL_MORE_CSS = ".bk td:nth-of-type(7)";
    public static final String TOTAL_LESS_CSS = ".bk td:nth-of-type(8)";
    public static final String WIN_FIRST_CSS = ".bk td:nth-of-type(9)";
    public static final String DRAW_CSS = ".bk td:nth-of-type(10)";
    public static final String WIN_SECOND_CSS = ".bk td:nth-of-type(11)";
    public static final String WIN_FIRST_DRAW_CSS = ".bk td:nth-of-type(12)";
    public static final String SOMEONE_WIN_CSS = ".bk td:nth-of-type(13)";
    public static final String WIN_SECOND_DRAW_CSS = ".bk td:nth-of-type(14)";
    public static final String INDIVIDUAL_TOTAL_FIRST_CSS = ".bk td:nth-of-type(15) b:nth-of-type(1)";
    public static final String INDIVIDUAL_TOTAL_SECOND_CSS = ".bk td:nth-of-type(15) b:nth-of-type(2)";
    public static final String INDIVIDUAL_TOTAL_FIRST_MORE_CSS = ".bk td:nth-of-type(16) u:nth-of-type(1)";
    public static final String INDIVIDUAL_TOTAL_SECOND_MORE_CSS = ".bk td:nth-of-type(16) u:nth-of-type(2)";
    public static final String INDIVIDUAL_TOTAL_FIRST_LESS_CSS = ".bk td:nth-of-type(17) u:nth-of-type(1)";
    public static final String INDIVIDUAL_TOTAL_SECOND_LESS_CSS = ".bk td:nth-of-type(17) u:nth-of-type(2)";

    private ParimatchConstants() {

    }
}
