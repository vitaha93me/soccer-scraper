package org.soccer.scraper;

import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.soccer.entities.Event;
import org.soccer.services.JsoupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Vitalii
 */
@Component
public class MyScoreScraper {

    @Autowired
    private JsoupService jsoupService;

    private static final String LIGA_CSS = "table.soccer";
    private static final String LIGA_NAME_CSS = "thead .country";
    private static final String LIGA_LINES_CSS = "tbody tr";
    private static final String START_TIME_CSS = ".cell_ad.startTime";
    private static final String MATCH_STATUS_CSS = ".cell_aa.timer";
    private static final String TEAM_HOME_CSS = ".cell_ab.team-home";
    private static final String TEAM_AWAY_CSS = ".cell_ac.team-away";
    private static final String SCORE_CSS = ".cell_sa.score";


    public List<Event> scrape() {
        try (final WebClient webClient = new WebClient()) {
            final HtmlPage page = webClient.getPage("http://www.myscore.com.ua/");
            webClient.waitForBackgroundJavaScript(60000);
            final String pageAsXml = page.asXml();
            Document document = jsoupService.parse(pageAsXml);

            List<Event> eventList = new ArrayList<>();
            for (Element liga : document.select(LIGA_CSS)) {

                String ligaName = liga.select(LIGA_NAME_CSS).text();

                for (Element line : liga.select(LIGA_LINES_CSS)) {
                    Event event = parseEventLine(line);
                    event.setLiga(ligaName);
                    eventList.add(event);
                }

            }
            return eventList;

        } catch (IOException e) {
            e.printStackTrace();
        }
        return Collections.emptyList();
    }

    private Event parseEventLine(Element line) {
        String startTime = line.select(START_TIME_CSS).text();
        String matchStatus = line.select(MATCH_STATUS_CSS).text();
        String teamHome = line.select(TEAM_HOME_CSS).text();
        String teamAway = line.select(TEAM_AWAY_CSS).text();
        String score = line.select(SCORE_CSS).text();

        Event event = new Event();
        event.setStartTime(startTime);
        event.setMatchStatus(matchStatus);
        event.setTeamHome(teamHome);
        event.setTeamAway(teamAway);
        event.setScore(score);
        return event;
    }
}
