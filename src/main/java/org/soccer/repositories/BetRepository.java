package org.soccer.repositories;

import org.soccer.entities.Bet;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * @author vitalii
 */
@RepositoryRestResource
public interface BetRepository extends PagingAndSortingRepository<Bet, Long> {

}
