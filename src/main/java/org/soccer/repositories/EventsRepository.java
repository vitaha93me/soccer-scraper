package org.soccer.repositories;

import org.soccer.entities.Event;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * @author vitalii
 */
@RepositoryRestResource
public interface EventsRepository extends JpaRepository<Event, Long> {

}