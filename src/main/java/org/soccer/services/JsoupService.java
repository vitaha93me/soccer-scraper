package org.soccer.services;

import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.stereotype.Service;

import java.io.IOException;

import static java.lang.Thread.sleep;

/**
 * @author vitalii
 */
@Service
public class JsoupService {

    private Logger logger = Logger.getLogger(JsoupService.class);

    private String userAgent = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36";
    private String accept = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8";

    public Document parse(String html) {
        return Jsoup.parse(html, "utf8");
    }

    public Document getDocument(String url) {
        try {
            logger.info("Fetching url:\t" + url);
            return Jsoup
                    .connect(url)
                    .maxBodySize(0)
                    .userAgent(userAgent)
                    .header("Accept", accept)
                    .timeout(120000)
                    .get();
        } catch (IOException e) {
            logger.error("Error fetching url: " + url, e);
            return getDocument(1, 3000, url);
        }
    }

    private Document getDocument(int errorCount, int latency, String url) {
        try {
            sleep(latency);
            logger.info("Re scraping url:\t" + url);
            return Jsoup.connect(url).userAgent(userAgent).ignoreContentType(true).timeout(15000).maxBodySize(0).get();
        } catch (IOException | InterruptedException e) {
            if (errorCount > 10) {
                logger.error("Error re scraping url: " + url, e);
                return new Document("");
            }
            latency += 2000;
            return getDocument(++errorCount, latency, url);
        }
    }
}
