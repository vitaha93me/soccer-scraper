package org.soccer.entities;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author vitalii
 */
@Table
@Entity
@Setter
@Getter
public class Event {

    @Id
    @GeneratedValue
    private Long id;
    private String startTime;
    private String matchStatus;

    private String teamHome;
    private String teamAway;
    private String score;

    private String liga;

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toStringExclude(this, "id");
    }
}
