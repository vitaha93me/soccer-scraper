package org.soccer.entities;

import lombok.Getter;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.math.NumberUtils;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.text.ParseException;

/**
 * @author vitalii
 */
@Table
@Entity
@Getter
public class Bet {

    @Id
    @GeneratedValue

    private Long id;
    private String date;
    private String liga;
    private String teamFirst;
    private String teamSecond;


    private double foraFirst;
    private double foraSecond;
    private double foraFirstKf;
    private double foraSecondKf;
    private double total;
    private double totalMore;
    private double totalLess;

    private double individualTotalFirst;
    private double individualTotalMoreFirst;
    private double individualTotalLessFirst;

    private double individualTotalSecond;
    private double individualTotalMoreSecond;
    private double individualTotalLessSecond;

    private double winFirst;
    private double winSecond;
    private double draw;

    private double winFirstOrDraw;
    private double winSecondOrDraw;
    private double someoneWin;

    public void setDate(String date) throws ParseException {
        this.date = date;
    }

    public void setLiga(String liga) {
        this.liga = liga;
    }

    public void setTeamFirst(String teamFirst) {
        this.teamFirst = teamFirst;
    }

    public void setTeamSecond(String teamSecond) {
        this.teamSecond = teamSecond;
    }

    public void setForaFirst(String foraFirst) {
        this.foraFirst = NumberUtils.toDouble(foraFirst, 0);
    }

    public void setForaSecond(String foraSecond) {
        this.foraSecond = NumberUtils.toDouble(foraSecond, 0);
    }

    public void setForaFirstKf(String foraFirstKf) {
        this.foraFirstKf = NumberUtils.toDouble(foraFirstKf, 0);
    }

    public void setForaSecondKf(String foraSecondKf) {
        this.foraSecondKf = NumberUtils.toDouble(foraSecondKf, 0);
    }

    public void setTotal(String total) {
        this.total = NumberUtils.toDouble(total, 0);
    }

    public void setTotalMore(String totalMore) {
        this.totalMore = NumberUtils.toDouble(totalMore, 0);
    }

    public void setTotalLess(String totalLess) {
        this.totalLess = NumberUtils.toDouble(totalLess, 0);
    }

    public void setIndividualTotalFirst(String individualTotalFirst) {
        this.individualTotalFirst = NumberUtils.toDouble(individualTotalFirst, 0);
    }

    public void setIndividualTotalMoreFirst(String individualTotalMoreFirst) {
        this.individualTotalMoreFirst = NumberUtils.toDouble(individualTotalMoreFirst, 0);
    }

    public void setIndividualTotalLessFirst(String individualTotalLessFirst) {
        this.individualTotalLessFirst = NumberUtils.toDouble(individualTotalLessFirst, 0);
    }

    public void setIndividualTotalSecond(String individualTotalSecond) {
        this.individualTotalSecond = NumberUtils.toDouble(individualTotalSecond, 0);
    }

    public void setIndividualTotalMoreSecond(String individualTotalMoreSecond) {
        this.individualTotalMoreSecond = NumberUtils.toDouble(individualTotalMoreSecond, 0);
    }

    public void setIndividualTotalLessSecond(String individualTotalLessSecond) {
        this.individualTotalLessSecond = NumberUtils.toDouble(individualTotalLessSecond, 0);
    }

    public void setWinFirst(String winFirst) {
        this.winFirst = NumberUtils.toDouble(winFirst, 0);
    }

    public void setWinSecond(String winSecond) {
        this.winSecond = NumberUtils.toDouble(winSecond, 0);
    }

    public void setDraw(String draw) {
        this.draw = NumberUtils.toDouble(draw, 0);
    }

    public void setWinFirstOrDraw(String winFirstOrDraw) {
        this.winFirstOrDraw = NumberUtils.toDouble(winFirstOrDraw, 0);
    }

    public void setWinSecondOrDraw(String winSecondOrDraw) {
        this.winSecondOrDraw = NumberUtils.toDouble(winSecondOrDraw, 0);
    }

    public void setSomeoneWin(String someoneWin) {
        this.someoneWin = NumberUtils.toDouble(someoneWin, 0);
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toStringExclude(this, "id");
    }
}
