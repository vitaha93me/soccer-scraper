package org.soccer;

import org.soccer.entities.Bet;
import org.soccer.entities.Event;
import org.soccer.repositories.BetRepository;
import org.soccer.repositories.EventsRepository;
import org.soccer.scraper.MyScoreScraper;
import org.soccer.scraper.PariMatchScraper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;

/**
 * @author Vitalii
 */
@Component
public class Scheduler {

    private final PariMatchScraper pariMatchScraper;

    private final BetRepository betRepository;

    private final MyScoreScraper scoreScraper;

    private final EventsRepository eventsRepository;

    @Autowired
    public Scheduler(PariMatchScraper pariMatchScraper, BetRepository betRepository, MyScoreScraper scoreScraper, EventsRepository eventsRepository) {
        this.pariMatchScraper = pariMatchScraper;
        this.betRepository = betRepository;
        this.scoreScraper = scoreScraper;
        this.eventsRepository = eventsRepository;
    }

    @Scheduled(fixedDelay = 60000)
    public void updateParimatch() {
        try {
            List<Bet> scrape = pariMatchScraper.scrape();
            betRepository.deleteAll();
            betRepository.save(scrape);
            System.out.println("Updated!!!");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //    @Scheduled(fixedDelay = 60000)
    public void updateMyScore() {
        List<Event> scrape = scoreScraper.scrape();
        eventsRepository.deleteAll();
        eventsRepository.save(scrape);
        System.out.println("Updated!!!");
    }
}
